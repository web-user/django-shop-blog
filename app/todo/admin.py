from django.contrib import admin
from .models import Project, Todo, Post

# Register your models here.
admin.site.register(Project)
admin.site.register(Todo)



class PostAdmin(admin.ModelAdmin):

    list_display = ('name', 'slug', 'author', 'updated', 'status')
    # readonly_fields = ('slug',)

    search_fields = ('name', 'description')


    class Meta:
        model = Post

admin.site.register(Post, PostAdmin)