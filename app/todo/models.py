from django.core.files import File
import os
import urllib
from django.urls import reverse
from django.conf import settings
from django.db import models
from django.utils import timezone

from myproject.utils import unique_slug_generator, get_filename
from django.db.models.signals import pre_save, post_save

User  = settings.AUTH_USER_MODEL


class PublishedManager(models.Manager):

    def get_queryset(self):
        return super(PublishedManager, self).get_queryset()\
                .filter(status='published')



class Post(models.Model):
    STATUS_CHOICES = (
        ('draft', 'Draft'),
        ('published', 'Published'),
        ('private', 'Private'),
    )
    name = models.CharField(max_length=255)
    slug = models.SlugField(max_length=255, blank=True, unique=True)
    author = models.CharField(max_length=170)
    body = models.TextField()
    photo = models.ImageField(upload_to='posts/%Y/%m/', blank=True, null=True, default=None)
    publish = models.DateTimeField(default=timezone.now)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    status = models.CharField(max_length=10, choices=STATUS_CHOICES, default='draft')

    objects = models.Manager()
    published = PublishedManager()

    class Meta:
        ordering = ('-publish',)

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        #return "/products/{slug}/".format(slug=self.slug)
        return reverse("parsersapi:post_detail", kwargs={"slug": self.slug})



def post_pre_save_receiver(sender, instance, *args, **kwargs):

    if not instance.slug:
        instance.slug = unique_slug_generator(instance)

pre_save.connect(post_pre_save_receiver, sender=Post)


class Project(models.Model):
    title = models.CharField(max_length=250)
    color = models.TextField()

    objects = models.Manager()

    def __str__(self):
        return self.title


class Todo(models.Model):
    PRIORIT_CHOICES = (
        ('high', 'High Priority'),
        ('medium', 'Medium Priority'),
        ('low', 'Low Priority'),
    )
    project = models.ForeignKey(Project, on_delete=models.CASCADE, related_name='todos')
    title = models.CharField(max_length=250)
    todo_priority = models.CharField(max_length=10, choices=PRIORIT_CHOICES, default='high')
    date_todo = models.DateField()
    completed_tasks = models.BooleanField(default=False)
    album_logo = models.FileField()

    objects = models.Manager()

    def __str__(self):
        return  self.title






