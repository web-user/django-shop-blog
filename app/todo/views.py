from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy, reverse
from django.views import View

from .forms import LoginForm, TodoFormSet, TodoForm, TodoFilter
from django.shortcuts import redirect, render
from django.contrib.auth import logout
from .models import Post, Project, Todo
from .serializers import TodoSerializer
from django.contrib.auth.forms import UserCreationForm

import django_filters.rest_framework
import django_filters

from rest_framework import routers, serializers, viewsets, filters

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import generics
from django.views.generic import FormView, ListView, TemplateView
from django.views.generic.edit import CreateView, UpdateView, DeleteView

from django.contrib import messages



class TodoListAPI(generics.ListAPIView):
    queryset = Todo.objects.order_by('date_todo')
    serializer_class = TodoSerializer
    filter_class = TodoFilter
    search_fields = ('title',)
    ordering_fields = ('date_todo', 'title', 'status_display')
    filter_backends = (
        django_filters.rest_framework.DjangoFilterBackend,
        filters.SearchFilter,
        filters.OrderingFilter,
    )


class ProjectListView(LoginRequiredMixin, CreateView):
    form_class = TodoForm
    template_name = 'todo/content.html'
    success_url = reverse_lazy('todo:dashboard')

    def get_context_data(self, **kwargs):
        # kwargs['projects'] = Project.objects.exclude(todos__completed_tasks=True)


        kwargs['projects'] = Project.objects.all()
        kwargs['page_title'] = 'Page - Dashboard'
        kwargs['project_todos'] = Todo.objects.exclude(completed_tasks=True)
        # print(request.GET)
        return super().get_context_data(**kwargs)

    def form_valid(self, form):
        return super().form_valid(form)

    def form_invalid(self, form):
        print("Invalid data")
        return super().form_invalid(form)


class CompletedListView(LoginRequiredMixin, CreateView):
    form_class = TodoForm
    template_name = 'todo/content.html'
    success_url = reverse_lazy('todo:dashboard')

    def get_context_data(self, **kwargs):
        # kwargs['projects'] = Project.objects.exclude(todos__completed_tasks=True)
        kwargs['projects'] = Project.objects.all()
        kwargs['page_title'] = 'Page - Dashboard'
        kwargs['completed'] = True
        kwargs['project_todos'] = Todo.objects.exclude(completed_tasks=False)
        # print(request.GET)
        return super().get_context_data(**kwargs)

    def form_valid(self, form):
        return super().form_valid(form)

    def form_invalid(self, form):
        print("Invalid data")
        return super().form_invalid(form)


class ProjectCreate(CreateView):
    model = Project
    fields = ['title', 'color']
    success_url = reverse_lazy('todo:dashboard')

    def form_valid(self, form):
        return super().form_valid(form)

    def form_invalid(self, form):
        print("Invalid data")
        messages.error(self.request, 'Invalid data')
        return redirect(reverse('todo:dashboard'))


class ProjecUpdate(UpdateView):
    model = Project
    fields = ['title', 'color']
    success_url = reverse_lazy('todo:dashboard')


class ProjecDelete(DeleteView):
    model = Project
    success_url = reverse_lazy('todo:dashboard')

    def delete(self, request, *args, **kwargs):
        project = Project.objects.get(pk=kwargs['pk'])
        todos = project.todos.all()
        if todos.filter(completed_tasks=False):
            messages.error(request, 'Oops, something bad happened')
            return redirect(reverse('todo:dashboard'))
        else:
            return super().delete(request, *args, **kwargs)


class TodoDelete(DeleteView):
    model = Todo
    success_url = reverse_lazy('todo:dashboard')


class TodoUpdate(UpdateView):
    model = Todo
    fields = ['title', 'completed_tasks', 'todo_priority']
    success_url = reverse_lazy('todo:dashboard')

