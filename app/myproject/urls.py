"""mysite URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path
from . import views

from django.conf import settings
from django.conf.urls.static import static
from django.contrib.auth import views as auth_views



urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('todo.urls', namespace="todo")),
    path('', include('orders.urls', namespace="orders")),
    path('', include('products.urls', namespace="products")),
    path('', include('landing.urls', namespace="landing")),
    path('', include('parsersapi.urls', namespace="parsersapi")),
    path('', include('search.urls', namespace="search")),
    path('', include('accounts.urls', namespace="accounts")),
    path('', include('billing.urls', namespace="billing")),
    path('', include('marketing.urls', namespace="marketing")),
    path('', include('analytics.urls', namespace="analytics")),

    # path('auth/social', auth_social.home, name="auth-social"),

    path('auth/social/', include('social_django.urls', namespace="social")),

    # path('', include('django.contrib.auth.urls', namespace='auth')),

    # url('', include('django.contrib.auth.urls', namespace='auth')),
    # url('', include('social.apps.django_app.urls', namespace='social')),

    path('accounts/password/change/', auth_views.PasswordChangeView.as_view(), name='password_change'),
    path('accounts/password/change/done/', auth_views.PasswordChangeDoneView.as_view(), name='password_change_done'),
    path('accounts/password/reset/', auth_views.PasswordResetView.as_view(), name='password_reset'),
    path('accounts/password/reset/done/', auth_views.PasswordResetDoneView.as_view(), name='password_reset_done'),
    path('accounts/reset/<uidb64>/<token>/', auth_views.PasswordResetConfirmView.as_view(), name='password_reset_confirm'),
    path('accounts/password/reset/complete/', auth_views.PasswordResetCompleteView.as_view(), name='password_reset_complete'),

    # path('', views.index, name='home'),

]


if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
