from django.http import HttpResponse

# import the logging library
import logging

# Get an instance of a logger
logger = logging.getLogger(__name__)



def index(request):
    qust = ''
    start_project_list = ['My project', 'Web network', 'Version: 2.1']
    output = ', '.join([n for n in start_project_list])
    return HttpResponse(output)

