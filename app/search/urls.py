from django.urls import path, include
from rest_framework.urlpatterns import format_suffix_patterns
from search import views


urlpatterns = [

    path('search/', views.SearchProductView.as_view(), name='search'),
]

app_name = 'search'

urlpatterns = format_suffix_patterns(urlpatterns)