from django.shortcuts import render
import re
from django.views.generic import FormView, ListView, TemplateView, DetailView
from products.models import Product, ProductCategory
# Create your views here.


class SearchProductView(ListView):
    model = Product
    template_name = 'search/content.html'

    def get_context_data(self, **kwargs):
        search_text = self.request.GET.get('q')

        kwargs['query'] = search_text

        if search_text is not None:
            kwargs['products'] = Product.objects.search(search_text)
        else:
            kwargs['products'] = Product.objects.all()


        kwargs['page_title'] = re.sub(r'[/%-@*]', ' ', self.request.path)

        kwargs['products_category'] = ProductCategory.objects.filter(is_active=True)

        return super().get_context_data(**kwargs)