from django import forms
from .models import BillingAdress

class AddressForm(forms.ModelForm):
    """
    User-related CRUD form
    """
    class Meta:
        model = BillingAdress
        fields = [
            # 'billing_profile',
            # 'address_type',
            'address_line_1',
            'address_line_2',
            'city',
            'country',
            'state',
            'postal_code'
        ]
