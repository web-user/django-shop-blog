from django.contrib import admin
from .models import BillingProfile, BillingAdress, Charge

admin.site.register(BillingProfile)
admin.site.register(BillingAdress)
admin.site.register(Charge)