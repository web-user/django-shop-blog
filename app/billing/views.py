from django.conf import settings
from django.http import JsonResponse, HttpResponse
from django.shortcuts import render

import stripe
stripe.api_key = settings.STRIPE_SECRET_KEY
STRIPE_PUB_KEY = settings.STRIPE_PUB_KEY

def payment_method_view(request):
    return render(request, 'orders/billing/payment-method.html', {"publish_key": STRIPE_PUB_KEY})


def payment_method_createview(request):
    if request.method == 'POST' and request.is_ajax():
        print(request.POST)
        return JsonResponse({"message": "Done"})
    raise HttpResponse("error", status=401)