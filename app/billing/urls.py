from django.urls import path, include
from rest_framework.urlpatterns import format_suffix_patterns

from . import views

urlpatterns = [
    path('billing/payment-method/', views.payment_method_view, name=''),
    path('billing/payment-method/create/', views.payment_method_createview, name=''),

]

app_name = 'billing'



urlpatterns = format_suffix_patterns(urlpatterns)