jQuery(document).ready(function ($) {
  $(document).ready(function () {
    localStorage.setItem('namekey', 'byba ryba')

    console.log('fff bbb SSSSVVWW !!!')

    $(function () {
      var my_home = window.location.pathname

      var url = window.location.pathname,
        urlRegExp = new RegExp(url.replace(/\/$/, '') + '$') // create regexp to match current url pathname and remove trailing slash if present as it could collide with the link in navigation in case trailing slash wasn't present there
        // now grab every link from the navigation
      $('.main-menu a').each(function () {
        // and test its normalized href against the url pathname regexp

        if (urlRegExp.test(this.href.replace(/\/$/, ''))) {
          if (my_home != '/') {
            $(this).closest('li').addClass('active-menu')
          }
        }
      })
    })

    $('.image-reg-False').each(function () {
      console.log($(this).attr('src'))
      var text = $(this).attr('src').replace('&, ', '')
      $(this).attr('src', text)
      console.log(text + 'wow')
    })

    // Get Category
    $('.checked-category').click(function () {
      if ($(this).is(':checked')) {
        console.log($(this).val())
      }
    })

    // Form login
    jQuery(document).ready(function ($) {
      tab = $('.tabs h3 a')

      tab.on('click', function (event) {
        event.preventDefault()
        tab.removeClass('active')
        $(this).addClass('active')

        tab_content = $(this).attr('href')
        $('div[id$="tab-content"]').removeClass('active')
        $(tab_content).addClass('active')
      })
    })

    // Checkout Price
    $(document).ready(function () {
      function updatePrice () {
        var sum, num, price, product_total, sum_total

        var element = $(this)
        var id_product = element.attr('product_id')
        var data_id = element.attr('data-id')

        var id_product_car = $('.product_id_' + data_id).val()

        console.log(' Prod: ' + id_product)

        if (id_product) {
          console.log(id_product)
          sum, num = parseInt($('#' + id_product).val(), 10)
          price = parseFloat($(".price-in[data-price='" + id_product + "'] span").text())

          product_total = id_product
        } else if (data_id) {
          console.log(data_id)
          sum, num = parseInt($('#' + data_id).val(), 10)
          price = parseFloat($(".price-in[data-price='" + data_id + "'] span").text())

          product_total = data_id
        }

        console.log(num)

        var is_delete = false

        var id_product = $(this).data('id')

        // console.log(numb_buy)

        console.log(id_product_car)

        var data = {}
        data.product_id = id_product_car
        data.nmb = num
        var csrf_token = $('[name="csrfmiddlewaretoken"]').val()
        data['csrfmiddlewaretoken'] = csrf_token

        if (is_delete) {
          data['is_delete'] = true
        }

        var url = $('#url-cart-product').val()

        console.log(url)

        console.log(data)
        $.ajax({
          url: url,
          type: 'POST',
          data: data,
          cache: true,
          success: function (data) {
            console.log('OK')

            console.log(data)
            // console.log(data.products.id_image + 'test!!')
            console.log(data.products + 'test!!')
          },
          error: function () {
            swal('', 'is Not added to cart ! ', 'error')
            console.log('error')
          }
        })

        // console.log(price)

        sum = num * price
        sum_total = 0

        $(".total-price[data-total='" + product_total + "'] span").text(sum.toFixed(2))

        $('.table-shopping-cart .total-price span').each(function (index) {
          sum_total += parseFloat($(this).text())
        })

        console.log(sum_total)

        $('.total-car-checkout').text(sum_total)
      }

      $(document).on('change, mouseup, keyup, keyup mouseup, click', '.table-shopping-cart .num-product, .table-shopping-cart .btn-num-product-down, .table-shopping-cart .btn-num-product-up', updatePrice)
    })

    // Ajax
    $(document).on('click', '.load-more-product:not(.loading)', function () {
      console.log('wow !!!')

      var loadMoreButton = this
      var page = parseInt(loadMoreButton.dataset.page, 10)
      var newPage = page + 1

      var ajaxurl = $(this).data('url')

      $(this).addClass('loading').find('.text').slideUp(320)
      $(this).find('.fa-spinner').addClass('spin')

      $.ajax({
        url: ajaxurl,
        method: 'GET',
        data: {
          'q': '',
          'res': 'ASB'
        },
        success: function (res) {
          // $('#image-res').hide();
          console.log(res)
          console.log(res.next)
          // console.log(loadMoreButton.dataset)
          // loadMoreButton.dataset.page = newPage
          $('.product-content-loader').append(res.results)

          setTimeout(function () {
            $(loadMoreButton).removeClass('loading').find('.text').slideDown(320)
            $(loadMoreButton).find('.fa-spinner').removeClass('spin')

            $('.product-content-loader .product').addClass('reveal')

            revealPosts()
          }, 1000)
        },
        error: function (res) {
          console.log(res)
        }
      })
    })

    $(document).on('click', '.header-cart-item-img', function (e) {
      e.preventDefault()

      var is_delete = true

      var id_product = $(this).data('id')
      var nmb = 0

      var data = {}
      data.product_id = id_product
      data.nmb = nmb
      var csrf_token = $('.form-buy-product [name="csrfmiddlewaretoken"]').val()
      data['csrfmiddlewaretoken'] = csrf_token

      if (is_delete) {
        data['is_delete'] = true
      }

      console.log(data)

      var url = $('#url-cart-product').val()

      $.ajax({
        url: url,
        type: 'POST',
        data: data,
        cache: true,
        success: function (data) {
          console.log('OK')

          $('.header-cart-wrapitem>.header-cart-item').remove()
          $('.icon-header-noti.js-show-cart').attr('data-notify', '')
          $('.header-cart-total').text()
          $('.header-cart-total').text('Total: $' + data.total_basket)
          $('.icon-header-noti.js-show-cart').attr('data-notify', data.products_total_nmb)
          console.log(data)

          $.each(data.products, function (k, v) {
            var clone = $('#products-template-main>.header-cart-item').clone()
            clone.find('.header-cart-item-txt .header-cart-item-name').text(v.name)
            clone.find('.header-cart-item-info').text(v.nmb + ' x $' + v.price_per_item)
            clone.find('.header-cart-item-img img').attr('src', v.id_image)

            $('.header-cart-wrapitem').prepend(clone)
          })
        },
        error: function () {
          console.log('error')
        }
      })
    })

    $('.form-buy-product').on('submit', function (e) {
      e.preventDefault()

      var is_delete = false

      var form_bay = $('.form-buy-product')

      var numb_buy = $('.num-product').val()

      var image_buy = $('.image-data-get').attr('src')

      var id_product = $(this).data('id')

      var name_product = $(this).data('name')

      var price_product = $(this).data('price')

      console.log(numb_buy)

      console.log(id_product)

      console.log(image_buy)

      var data = {}
      data.product_id = id_product
      data.nmb = numb_buy
      var csrf_token = $('.form-buy-product [name="csrfmiddlewaretoken"]').val()
      data['csrfmiddlewaretoken'] = csrf_token

      if (is_delete) {
        data['is_delete'] = true
      }

      var url = $('.form-buy-product').attr('action')

      console.log(url)

      console.log(data)
      $.ajax({
        url: url,
        type: 'POST',
        data: data,
        cache: true,
        success: function (data) {
          console.log('OK')

          console.log(data)

          swal(name_product, 'is added to cart ! ', 'success')

          $('.header-cart-wrapitem>.header-cart-item').remove()
          $('.icon-header-noti.js-show-cart').attr('data-notify', '')

          $('.header-cart-total').text()

          $('.header-cart-total').text('Total: $' + data.total_basket)

          $('.icon-header-noti.js-show-cart').attr('data-notify', data.products_total_nmb)

          // console.log(data.products.id_image + 'test!!')
          console.log(data.products + 'test!!')

          $.each(data.products, function (k, v) {
            var clone = $('#products-template-main>.header-cart-item').clone()
            clone.find('.header-cart-item-txt .header-cart-item-name').text(v.name)
            clone.find('.header-cart-item-info').text(v.nmb + ' x $' + v.price_per_item)

            clone.find('.header-cart-item-img').attr('data-id', v.id)

            clone.find('.header-cart-item-img img').attr('src', image_buy)

            // console.log(v.id_image['fields'] + 'test!!')

            $('.header-cart-wrapitem').prepend(clone)
          })
        },
        error: function () {
          swal(name_product, 'is Not added to cart ! ', 'error')
          console.log('error')
        }
      })
    })

    $('#form-parser-api').on('submit', function (e) {
      e.preventDefault()

      var numb_count = $('#numb_count').val()

      var data = {}
      data.news_api = 'True'
      data.nmb = 1
      data.numb_count = numb_count
      var csrf_token = $('#form-parser-api [name="csrfmiddlewaretoken"]').val()
      data['csrfmiddlewaretoken'] = csrf_token

      var url = $('#form-parser-api').attr('action')

      console.log(url)

      console.log(data)
      $.ajax({
        url: url,
        type: 'POST',
        data: data,
        cache: true,
        success: function (data) {
          console.log('OK')
          console.log(data)
        },
        error: function () {
          console.log('error')
        }
      })
    })

    // Contact Form
    $('#form-contact').on('submit', function (e) {
      e.preventDefault()
      var formdata = $(this)
      var data = formdata.serialize()
      var url = formdata.attr('action')

      console.log(data)

      $.ajax({
        url: url,
        type: 'POST',
        data: data,
        cache: true,
        success: function (data) {
          console.log('OK')
          console.log(data)
          swal('', data.message, 'success')
          formdata[0].reset()
        },
        error: function (error) {
          console.log(error)
          var jsondata = error.responseJSON
          var msg = ''
          // $(formdata).find('label').text(':')

          $.each(jsondata, function (key, value) {
            // $(formdata, ' label').find('[for="id_' + key + '"]').text(value[0].message)
            msg += key + ': ' + value[0].message
          })
          console.log(msg)
          swal('', msg, 'error')
          console.log('error')
        }
      })
    })

    /* ------------------Testing----------------------- */

    function renderChart (id, data, labels) {
      // var ctx = document.getElementById("myChart").getContext('2d');
      var ctx = $('#' + id)
      var myChart = new Chart(ctx, {
        type: 'line',
        data: {
          labels: labels,
          datasets: [{
            label: 'Sales',
            data: data,
            backgroundColor: 'rgba(0, 158, 29, 0.45)',
            borderColor: 'rgba(0, 158, 29, 1)'
          }]
        },
        options: {
          scales: {
            yAxes: [{
              ticks: {
                beginAtZero: true
              }
            }]
          },
          backgroundColor: 'rgba(75, 192, 192, 1)'
        }
      })
    }

    function getSalesData (id, type) {
      console.log('Asss 123')
      var url = '/analytics/sales/data/'
      var method = 'GET'
      var data = {'type': type}
      $.ajax({
        url: url,
        method: method,
        data: data,
        success: function (responseData) {
          renderChart(id, responseData.data, responseData.labels)
        },
        error: function (error) {
          $.alert('An error occurred')
        }
      })
    }
    var chartsToRender = $('.cfe-render-chart')

    $.each(chartsToRender, function (index, html) {
      var $this = $(this)
      console.log($this)
      if ($this.attr('id') && $this.attr('data-type')) {
        console.log('Asss Data')
        getSalesData($this.attr('id'), $this.attr('data-type'))
      }
    })

    /* ------------------Testing----------------------- */
    // var initialURL = tweetContainer.attr('data-url') || '/products/API/'
    // // console.log(initialURL)

    // function fetchTweets (url) {
    //   console.log('fetching')
    //   var fecthUrl
    //   if (!url) {
    //     fecthUrl = initialURL
    //   } else {
    //     fecthUrl = url
    //   }
    //   $.ajax({
    //     url: fecthUrl,
    //     data: {
    //       'q': query
    //     },
    //     method: 'GET',
    //     success: function (data) {
    //     // console.log(data)
    //       tweetList = data.results
    //       if (data.next) {
    //         nextTweetUrl = data.next
    //       } else {
    //         $('#loadmore').css('display', 'none')
    //       }
    //       parseTweets()
    //       updateHashLinks()
    //     },
    //     error: function (data) {
    //       console.log('error')
    //       console.log(data)
    //     }
    //   })
    // }

    // fetchTweets()

    /* ------------------Testing----------------------- */
  })
})
