from django.contrib import admin
from .models import ParserApi

# Register your models here.

class ParserApiAdmin(admin.ModelAdmin):

    list_display = [field.name for field in ParserApi._meta.fields]

    class Meta:
        model = ParserApi

admin.site.register(ParserApi, ParserApiAdmin)