from django.urls import path, include
from django.contrib.auth.views import login
from django.contrib.auth.views import logout
from django.contrib.auth.views import logout_then_login
from rest_framework.urlpatterns import format_suffix_patterns
from django.conf.urls import handler404, handler500

from . import views



urlpatterns = [
    path('parser-api/', views.ParserApiView.as_view(), name='parserapi'),
    path('parser_get/', views.parser_get, name='parser_get'),
    path('parser/API/', views.ParserApiRest.as_view(), name='parser_api'),
    path('csv_parse/', views.csv_parse, name='csv_parse'),
    path('blog/', views.PostsListView.as_view(), name='posts'),
    path('blog/<slug:slug>/', views.PostsDetailView.as_view(), name='post_detail'),

]

app_name = 'parsersapi'



urlpatterns = format_suffix_patterns(urlpatterns)