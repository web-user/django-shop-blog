from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.forms import inlineformset_factory
from .models import ParserApi

import django_filters


class ParserApiFilter(django_filters.FilterSet):

    date = django_filters.DateFilter(name='publish', lookup_expr='gte')
    end_date = django_filters.DateFilter(name='publish', lookup_expr='lte')

    class Meta:
        model = ParserApi
        fields = ('date', 'end_date' )


