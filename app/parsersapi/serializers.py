from .models import ParserApi
from rest_framework import routers, serializers, viewsets

class ParserApiSerializer(serializers.ModelSerializer):

    class Meta:
        model = ParserApi
        fields = ('id', 'title', 'address', 'email', 'publish',
                  'lat_lon', 'web_page')