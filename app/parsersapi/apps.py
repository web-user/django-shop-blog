from django.apps import AppConfig


class ParserApiConfig(AppConfig):
    name = 'parsersapi'
