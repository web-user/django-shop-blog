import requests
import time
import csv
import re

from django.core.files import File
from django.core.files.temp import NamedTemporaryFile

import requests

from django.shortcuts import render, get_object_or_404, redirect
from django.views.generic import ListView, DetailView, View
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import JsonResponse, HttpResponse
from .forms import ParserApiFilter
from .models import ParserApi
from todo.models import Post
from .serializers import ParserApiSerializer
import django_filters
from rest_framework import routers, serializers, viewsets, filters
from rest_framework import generics
from django.views.generic.edit import CreateView, UpdateView, DeleteView


def csv_parse(request):
    get_all = ParserApi.objects.all()

    # Create the HttpResponse object with the appropriate CSV header.
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="somefilename.csv"'
    writer = csv.writer(response)
    writer.writerow(['Email Address', 'First Name', 'Address'])

    for item in get_all:
        if item.email:
            writer.writerow([item.email, item.title, item.address])

    return response

class ParserApiRest(generics.ListAPIView):
    queryset = ParserApi.objects.order_by('publish')
    serializer_class = ParserApiSerializer
    filter_class = ParserApiFilter
    search_fields = ('title',)
    ordering_fields = ('publish', 'title', 'status_display')
    filter_backends = (
        django_filters.rest_framework.DjangoFilterBackend,
        filters.SearchFilter,
        filters.OrderingFilter,
    )


class ParserApiView(LoginRequiredMixin, CreateView):
    model = ParserApi
    fields = ['title', 'address', 'email', 'lat_lon', 'web_page']
    template_name = 'content_parser.html'

    def get_context_data(self, **kwargs):
        kwargs['parseapi'] = ParserApi.objects.all()
        kwargs['page_title'] = 'Page - Parser API'

        return super().get_context_data(**kwargs)

    def form_valid(self, form):
        return super().form_valid(form)

    def form_invalid(self, form):
        print("Invalid data")
        return super().form_invalid(form)


class PostsListView(ListView):
    model = Post
    template_name = 'posts/posts_content.html'

    def get_context_data(self, **kwargs):
        kwargs['posts'] = Post.objects.all()[:5]
        return super().get_context_data(**kwargs)


class PostsDetailView(DetailView):
    model = Post
    template_name = 'posts/content.html'

    def get_context_data(self, **kwargs):

        slug = self.kwargs.get('slug')

        kwargs['post'] = Post.objects.filter(slug=slug)
        kwargs['page_title'] = re.sub(r'[/%-@*]', ' ', slug)

        return super().get_context_data(**kwargs)


def parser_get(request):
    my_pars = dict()
    data = request.POST
    numb_count = data.get("numb_count")

    news_api = data.get("news_api")

    print(data)

    if news_api:
        print('Parsss ')
        """ https://newsapi.org/v2/everything?sources=the-new-york-times&apiKey=2dcef9b724484f7a867c42c7df001356&page=2 """
        url_pars = requests.get('https://newsapi.org/v2/everything?sources=the-new-york-times&apiKey=2dcef9b724484f7a867c42c7df001356' + numb_count).json()
        get_all_articles = url_pars
        my_pars['name'] = get_all_articles['articles']
        if get_all_articles['status'] == 'ok':
            for item in get_all_articles['articles']:
                if not item['author']:
                    user = request.user.full_name
                    print(request.user.full_name)
                else:
                    user = item['author']

                r = requests.get(item['urlToImage'])

                parser_save = Post.objects.create(name=item['title'], author=user, body=item['content'])

                img_temp = NamedTemporaryFile(delete=True)
                img_temp.write(r.content)
                img_temp.flush()

                parser_save.photo.save("image.jpg", File(img_temp), save=True)

    else:
        pass

        # url_pars = requests.get('https://newsapi.org/v2/everything?sources=the-new-york-times&apiKey=2dcef9b724484f7a867c42c7df001356&page=' + numb_count).json()
        # my_pars['name'] = url_pars
        #
        # for item in url_pars:
        #     par_email = ParserApi.objects.filter(email=item['email'])
        #
        #     if not par_email:
        #         adress_set = ' '.join([key.upper() + ': ' + val if val else '' for key, val in item['address'].items()])
        #         parser_save = ParserApi(title=item['title'], address=adress_set, email=item['email'],
        #                                 lat_lon=item['lat'], web_page=item['web_page'])
        #         parser_save.save()


    return JsonResponse(my_pars)