from django.contrib.auth.mixins import LoginRequiredMixin
from django.conf import settings
from django.http import JsonResponse, Http404
from .models import ProductInBasket, Order, ProductInOrder
from billing.models import BillingProfile, BillingAdress
from products.models import ProductImage, Product
from django.urls import reverse_lazy, reverse
from django.views.generic import FormView, ListView, TemplateView, DetailView, UpdateView
from django.shortcuts import render, redirect
from accounts.forms import LoginForm, GuestForm
from accounts.models import GuestEmail
from billing.forms import AddressForm
import stripe
stripe.api_key = settings.STRIPE_SECRET_KEY
STRIPE_PUB_KEY = settings.STRIPE_PUB_KEY


class OrderListView(LoginRequiredMixin, ListView):
    template_name = 'orders/list.html'

    def get_queryset(self):
        return Order.objects.by_request(self.request).not_created()


class OrderDetailView(LoginRequiredMixin, DetailView):
    template_name = 'orders/detail.html'

    def get_object(self):
        qs = Order.objects.by_request(self.request).filter(order_id=self.kwargs.get('order_id'))
        if qs.count() == 1:
            return qs.first()
        return Http404


class CartView(ListView):
    model = ProductInBasket
    template_name = 'orders/cart.html'

    def get_context_data(self, **kwargs):
        session_key = self.request.session.session_key
        products_in_basket = ProductInBasket.objects.filter(session_key=session_key, is_active=True, order__isnull=True)
        return super().get_context_data(**kwargs)


class CheckoutAddressView(FormView):
    template_name = ''
    form_class = AddressForm

    def form_valid(self, form):
        cleaned_data = form.cleaned_data
        instance = form.save(commit=False)

        billing_profile, billing_guest_profile_created = BillingProfile.objects.new_or_get(self.request)

        if billing_profile is not None:
            address_type = self.request.POST.get('address_type', 'shipping')
            instance.billing_profile = billing_profile
            instance.address_type = address_type
            instance.save()
            self.request.session[address_type + "_address_id"] = instance.id
        else:
            print("Error here")
            return redirect(reverse('orders:checkout'))

        return redirect(reverse('orders:checkout'))

    def form_invalid(self, form):
        return redirect(reverse('orders:checkout'))


def basket_adding(request):
    return_dict = dict()
    total_sum = 0

    session_key = request.session.session_key
    data = request.POST
    product_id = data.get("product_id")
    nmb = data.get("nmb")
    is_delete = data.get("is_delete")

    if is_delete == 'true':
        ProductInBasket.objects.filter(id=product_id).delete()
    else:

        print(product_id)

        print(data)

        print('how')

        print(session_key)

        new_product, created = ProductInBasket.objects.get_or_create(session_key=session_key, product_id=product_id,
                                                                     is_active=True, defaults={"nmb": nmb})

        print("No create ")
        if not created:
            print("No create ")
            new_product.nmb = int(nmb)
            new_product.save(force_update=True)

    products_in_basket = ProductInBasket.objects.filter(session_key=session_key, is_active=True, order__isnull=True)

    products_total_nmb = products_in_basket.count()
    return_dict["products_total_nmb"] = products_total_nmb

    products = [
        {
            "id": x.id,
            "name": x.product.name,
            "price_per_item": x.price_per_item,
            "nmb": x.nmb,
            "total": x.total_price
        }

        for x in products_in_basket
    ]

    for total in products_in_basket:
        total_sum += total.total_price

    return_dict["total_basket"] = total_sum
    return_dict["products"] = products

    return JsonResponse(return_dict, status=200)


def checkout_view(request):

    session_key = request.session.session_key
    order_obj = None
    login_form = LoginForm(request=request)
    guest_form = GuestForm()
    address_form = AddressForm()
    billing_address_form = AddressForm()

    billing_address_id = request.session.get("billing_address_id", None)
    shipping_address_id = request.session.get("shipping_address_id", None)

    billing_profile, billing_guest_profile_created = BillingProfile.objects.new_or_get(request)
    products_in_basket = ProductInBasket.objects.filter(session_key=session_key, is_active=True)



    if products_in_basket.count() == 0:
        return redirect('landing:home_landing')

    if billing_profile is not None:

        if request.user.is_authenticated:
            user = request.user.email
        else:
            user = GuestEmail.objects.get(session_key=session_key)
        order_obj, new_obj = Order.objects.new_or_get(billing_profile)

        if shipping_address_id:
            order_obj.shipping_address = BillingAdress.objects.get(id=shipping_address_id)
            del request.session["shipping_address_id"]
        if billing_address_id:
            order_obj.billing_address = BillingAdress.objects.get(id=billing_address_id)
            del request.session["billing_address_id"]

        if billing_address_id or shipping_address_id:
            order_obj.save()

        for product_id in products_in_basket:
            order_sq = order_obj.cart.filter(id=product_id.id)
            if not order_sq:
                order_obj.cart.add(product_id)

    if request.method == "POST":
        "check that order is done"
        is_prepared = order_obj.check_done()

        token = request.POST.get("token")

        if is_prepared:
            customer = stripe.Customer.retrieve(billing_profile.customer_id)
            stripe_card_response = customer.sources.create(source=token)
            stripe_id =  stripe_card_response.id

            did_charge, crg_msg = billing_profile.charge(order_obj, stripe_id)

            if did_charge:
                order_obj.mark_paid() # sort a signal for us
                session_key = request.session.session_key

                for product in products_in_basket:
                    ProductInOrder.objects.create(product_id=product.product.id, nmb=product.nmb,
                                                  price_per_item=product.price_per_item,
                                                  total_price=product.total_price,
                                                  order_id=order_obj.id)

                ProductInBasket.objects.filter(session_key=session_key).delete()

                return  JsonResponse({"url": '/order/success'}, status=200)

            else:
                return redirect("orders:checkout")

    context = {
        "object": order_obj,
        "billing_profile": billing_profile,
        "login_form": login_form,
        "guest_form": guest_form,
        "address_form": address_form,
        "billing_address_form": billing_address_form,
        "publish_key": STRIPE_PUB_KEY,

    }

    return render(request, "orders/checkout.html", context)


def checkout_done_view(request):
    return render(request, "orders/checkout-done.html", {})