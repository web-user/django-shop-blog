# Generated by Django 2.0.4 on 2018-09-26 12:34

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('orders', '0023_auto_20180926_1220'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='order',
            name='customer_email',
        ),
        migrations.RemoveField(
            model_name='order',
            name='customer_name',
        ),
    ]
