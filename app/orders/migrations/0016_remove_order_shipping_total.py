# Generated by Django 2.0.4 on 2018-08-16 07:40

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('orders', '0015_auto_20180816_0726'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='order',
            name='shipping_total',
        ),
    ]
