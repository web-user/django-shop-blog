import re
from .models import ProductInBasket


def getting_basket_info(request):

    total_sum = 0

    page_title = re.sub(r'[/%-@*]', ' ', request.path)

    # request.session.set_expiry(60)
    session_key = request.session.session_key
    if not session_key:
        #workaround for newer Django versions
        request.session["session_key"] = 123
        #re-apply value
        request.session.cycle_key()

    products_in_basket = ProductInBasket.objects.filter(session_key=session_key, is_active=True)
    products_total_nmb = products_in_basket.count()

    for total in products_in_basket:
        # total_sum += [total_count for total_count in range(total.nmb)]
        for total_count in range(total.nmb):
            total_sum += total.product.price

    total_basket = total_sum

    return locals()

