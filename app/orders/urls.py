from django.urls import path, include
from rest_framework.urlpatterns import format_suffix_patterns


from . import views

urlpatterns = [
    path('basket_adding/', views.basket_adding, name='basket_adding'),
    path('checkout/', views.checkout_view, name='checkout'),
    path('cart/', views.CartView.as_view(), name='cart'),
    path('checkout-address-shipping', views.CheckoutAddressView.as_view(), name='address_shipping'),
    path('order/success/', views.checkout_done_view, name='success'),
    path('orders/', views.OrderListView.as_view(), name='list'),
    path('orders/<order_id>/', views.OrderDetailView.as_view(), name='detail'),

]

app_name = 'orders'



urlpatterns = format_suffix_patterns(urlpatterns)