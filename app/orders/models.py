import math
import datetime
from django.db import models
from django.utils import timezone
from products.models import Product
from django.db.models.signals import pre_save, post_save, m2m_changed, pre_delete
from django.db.models import Count, Sum, Avg
from django.urls import reverse
from billing.models import BillingProfile, BillingAdress

from myproject.utils import unique_order_id_generator

ORDER_STATUS_CHOICES = (
    ('created', 'Created'),
    ('paid', 'Paid'),
    ('shipped', 'Shipped'),
    ('refunded', 'Refunded'),
)


class PublishedManager(models.Manager):

    def get_queryset(self):
        return super().get_queryset().filter(status='published')


class OrderManagerQuerySet(models.query.QuerySet):
    def recent(self):
        return self.order_by("-updated", "-created")

    def get_sales_breakdown(self):
        recent = self.recent().not_refunded()
        recent_data = recent.totals_data()
        shipped = recent.not_refunded().by_status(status='shipped')
        shipped_data = shipped.totals_data()
        paid = recent.by_status(status='paid')
        paid_data = paid.totals_data()
        data = {
            'recent': recent,
            'recent_data':recent_data,
            'shipped': shipped,
            'shipped_data': shipped_data,
            'paid': paid,
            'paid_data': paid_data
        }
        return data


    def by_weeks_range(self, weeks_ago=7, number_of_weeks=2):
        if number_of_weeks > weeks_ago:
            number_of_weeks = weeks_ago
        days_ago_start = weeks_ago * 7  # days_ago_start = 49
        days_ago_end = days_ago_start - (number_of_weeks * 7) #days_ago_end = 49 - 14 = 35
        start_date = timezone.now() - datetime.timedelta(days=days_ago_start)
        end_date = timezone.now() - datetime.timedelta(days=days_ago_end)
        return self.by_range(start_date, end_date=end_date)

    def by_range(self, start_date, end_date=None):
        if end_date is None:
            return self.filter(updated__gte=start_date)
        return self.filter(updated__gte=start_date).filter(updated__lte=end_date)

    def by_date(self):
        now = timezone.now() - datetime.timedelta(days=9)
        return self.filter(updated__day__gte=now.day)

    def totals_data(self):
        return self.aggregate(Sum("total"), Avg("total"))

    def by_status(self, status="shipped"):
        return self.filter(status=status)

    def not_refunded(self):
        return self.exclude(status='refunded')

    def by_request(self, request):
        billing_profile, created = BillingProfile.objects.new_or_get(request)
        return self.filter(billing_profile=billing_profile)

    def not_created(self):
        return self.exclude(status='created')


class OrderManager(models.Manager):

    def get_queryset(self):
        return OrderManagerQuerySet(self.model, using=self._db)

    def by_request(self, request):
        return self.get_queryset().by_request(request)

    def new_or_get(self, billing_profile):
        created = False
        qs = self.get_queryset().filter(
                billing_profile=billing_profile,
                is_active=True,
                status='created'
            )
        if qs.count() == 1:
            obj = qs.first()
        else:
            obj = self.model.objects.create(billing_profile=billing_profile)
            created = True
        return obj, created


class ProductInBasket(models.Model):
    session_key         = models.CharField(max_length=128, blank=True, null=True, default=None)
    product             = models.ForeignKey(Product, related_name='products', blank=True, null=True, default=None, on_delete=models.CASCADE)
    nmb                 = models.IntegerField(default=1)
    price_per_item      = models.DecimalField(max_digits=10, decimal_places=2, default=0)
    total_price         = models.DecimalField(max_digits=10, decimal_places=2, default=0) #price*nmb
    is_active           = models.BooleanField(default=True)
    created             = models.DateTimeField(auto_now_add=True, auto_now=False)
    updated             = models.DateTimeField(auto_now_add=False, auto_now=True)

    objects = models.Manager()

    def __str__(self):
        return "{}".format(self.product.name)

    def save(self, *args, **kwargs):
        print(" ADD ")
        print(self.product)
        price_per_item = self.product.price
        print('MMM')
        self.price_per_item = price_per_item
        self.total_price = int(self.nmb) * price_per_item

        super().save(*args, **kwargs)


class Order(models.Model):
    billing_profile     = models.ForeignKey(BillingProfile, blank=True, null=True, default=None, on_delete=models.CASCADE)
    order_id            = models.CharField(max_length=120, blank=True)  # AB31DE3
    shipping_address    = models.ForeignKey(BillingAdress, on_delete=models.CASCADE, related_name="shipping_address", null=True, blank=True)
    billing_address     = models.ForeignKey(BillingAdress, on_delete=models.CASCADE, related_name="billing_address", null=True, blank=True)
    shipping_address_final    = models.TextField(blank=True, null=True)
    billing_address_final     = models.TextField(blank=True, null=True)
    status              = models.CharField(max_length=120, default='created', choices=ORDER_STATUS_CHOICES)
    cart                = models.ManyToManyField(ProductInBasket, blank=True, default=None)
    total               = models.DecimalField(default=0.00, max_digits=100, decimal_places=2)
    comments            = models.TextField(blank=True, null=True, default='')
    customer_phone      = models.CharField(max_length=48, blank=True, null=True, default=None)
    created             = models.DateTimeField(auto_now_add=True, auto_now=False)
    updated             = models.DateTimeField(auto_now_add=False, auto_now=True)
    is_active           = models.BooleanField(default=True)

    objects = OrderManager()

    def __str__(self):
        return "Order {} {}".format(self.id, self.status)

    def get_shipping_status(self):
        if self.status == "refunded":
            return "Refunded order"
        elif self.status == "shipped":
            return "Shipped"
        else:
            return "Shipping Soon"

    def check_done(self):
        billing_profile = self.billing_profile
        billing_address = self.billing_address
        shipping_address = self.shipping_address
        total = self.total

        if billing_profile and shipping_address and billing_address and total > 0:
            return True
        return False

    def mark_paid(self):
        if self.check_done():
            self.status = "paid"
            self.save()
        return self.status


def pre_save_create_order_id(sender, instance, *args, **kwargs):
    if not instance.order_id:
        instance.order_id = unique_order_id_generator(instance)

    if instance.shipping_address and not instance.shipping_address_final:
        instance.shipping_address_final = instance.shipping_address.get_address()

    if instance.billing_address and not instance.billing_address_final:
        instance.billing_address_final = instance.billing_address.get_address()


pre_save.connect(pre_save_create_order_id, sender=Order)


def m2m_changed_order_receiver(sender, instance, action, *args, **kwargs):
    if action == 'post_add' or action == 'post_remove' or action == 'post_clear':
        products_in_order = instance.cart.all()

        order_total_price = 0
        for item in products_in_order:
            order_total_price += item.total_price

        instance.total = order_total_price
        instance.save()

m2m_changed.connect(m2m_changed_order_receiver, sender=Order.cart.through)


class ProductInOrder(models.Model):
    order = models.ForeignKey(Order, on_delete=models.CASCADE, related_name="order_product", blank=True, null=True, default=None)

    product = models.ForeignKey(Product, related_name="get_product", blank=True, null=True, default=None, on_delete=models.CASCADE)

    nmb = models.IntegerField(default=1)
    price_per_item = models.DecimalField(max_digits=10, decimal_places=2, default=0)
    total_price = models.DecimalField(max_digits=10, decimal_places=2, default=0)#price*nmb
    is_active = models.BooleanField(default=True)
    created = models.DateTimeField(auto_now_add=True, auto_now=False)
    updated = models.DateTimeField(auto_now_add=False, auto_now=True)

    objects = models.Manager()

    def __str__(self):
        return "%s" % self.product.name

    def save(self, *args, **kwargs):
        price_per_item = self.product.price
        self.price_per_item = price_per_item
        print (self.nmb)

        self.total_price = int(self.nmb) * price_per_item

        super(ProductInOrder, self).save(*args, **kwargs)


def product_in_order_post_save(sender, instance, created, **kwargs):
    order = instance.order
    all_products_in_order = ProductInOrder.objects.filter(order=order, is_active=True)

    order_total_price = 0
    for item in all_products_in_order:
        order_total_price += item.total_price

    instance.order.total_price = order_total_price
    instance.order.save(force_update=True)


post_save.connect(product_in_order_post_save, sender=ProductInOrder)






