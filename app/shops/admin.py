from django.contrib import admin
from .models import Shop, LocationShop



class LocationShopInline(admin.TabularInline):
    model = LocationShop
    extra = 0


class ShopAdmin(admin.ModelAdmin):
    list_display = [field.name for field in Shop._meta.fields]
    inlines = [LocationShopInline]

    class Meta:
        model = Shop

admin.site.register(Shop, ShopAdmin)
admin.site.register(LocationShop)
