from django.db import models


class PublishedManager(models.Manager):

    def get_queryset(self):
        return super(PublishedManager, self).get_queryset()\
                .filter(status='published')


class Shop(models.Model):
    title = models.CharField(max_length=64, blank=False, null=True, default=None)
    description = models.TextField(blank=True, null=True, default=None)
    is_active = models.BooleanField(default=True)
    created = models.DateTimeField(auto_now_add=True, auto_now=False)
    updated = models.DateTimeField(auto_now_add=False, auto_now=True)

    objects = models.Manager()

    def __str__(self):
        return "{}".format(self.title)


class LocationShop(models.Model):
    title = models.CharField(max_length=64, blank=False, null=True, default=None)
    latitude = models.DecimalField(max_digits=20, decimal_places=12, null=True, blank=True)
    longitude = models.DecimalField(max_digits=20, decimal_places=12, null=True, blank=True)
    shop = models.ForeignKey(Shop, on_delete=models.CASCADE, related_name='locations', blank=True, null=True, default=None)
    description = models.CharField(max_length=164,  blank=True, null=True, default=None)
    is_active = models.BooleanField(default=True)
    created = models.DateTimeField(auto_now_add=True, auto_now=False)
    updated = models.DateTimeField(auto_now_add=False, auto_now=True)

    objects = models.Manager()

    def __str__(self):
        return "{}".format(self.title)

