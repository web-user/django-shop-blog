from django.urls import path, include
from rest_framework.urlpatterns import format_suffix_patterns
from django.contrib.auth import views as auth_views

from accounts import views



urlpatterns = [
    path('login/', views.LoginFormView.as_view(), name='login'),
    path('register/quest/', views.GuestFormView.as_view(), name='quest_register'),
    path('registration/', views.RegisterFormView.as_view(), name='registration'),
    path('logout/', views.logout_view, name='logout'),

    path('account/', views.AccountHomeView.as_view(), name='account'),

    path('email/confirm/<key>/', views.AccountEmailActivateView.as_view(), name='email-activate'),



]

app_name = 'accounts'



urlpatterns = format_suffix_patterns(urlpatterns)