from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib import messages
from django.urls import reverse_lazy, reverse
from .forms import LoginForm, GuestForm, SignUpForm
from django.shortcuts import redirect, render
from django.views.generic import FormView, CreateView, DetailView, View
from django.views.generic.edit import FormMixin
from django.contrib.auth import logout
from .models import GuestEmail, EmailActivation
from .signals import user_logged_in
from django.utils.safestring import mark_safe
from myproject.mixins import RequestFormAttachMixin

class AccountHomeView(LoginRequiredMixin, DetailView):
    template_name = 'accounts/home.html'

    def get_object(self):
        return self.request.user


class AccountEmailActivateView(FormMixin, View):
    success_url = '/login/'

    def get(self, request, key, *args, **kwargs):
        qs = EmailActivation.objects.filter(key__iexact=key)
        confirm_qs = qs.confirmable()
        if confirm_qs.count() == 1:
            obj = qs.first()
            obj.activate()
            messages.success(request, "Your email has been confirmed. Please login.")
            return redirect("accounts:login")
        else:
            activated_qs = qs.filter(activated=True)
            if activated_qs.exists():
                reset_link = reverse("password_reset")
                msg = """Your email has already been confirmed
                Do you need to <a href="{link}">reset your password</a>?
                """.format(link=reset_link)
                messages.success(request, mark_safe(msg))
                return redirect("accounts:login")

        context = {'key': key}

        return render(request, 'registration/activation-error.html', context)

    def post(self, request, *args, **kwargs):
        # create form to receive an email
        form = self.get_form()
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)


class GuestFormView(FormView):
    template_name = 'accounts/auth.html'
    form_class = GuestForm

    def form_valid(self, form):
        cleaned_data = form.cleaned_data
        session_key = self.request.session.session_key
        email = cleaned_data.get("email")

        new_guest_email = GuestEmail.objects.filter(email=email).first()
        if not new_guest_email:
            new_guest_email = GuestEmail.objects.create(email=email, session_key=session_key)

        self.request.session['guest_email_id'] = new_guest_email.id
        return redirect(reverse('orders:checkout'))

    def form_invalid(self, form):
        return redirect(reverse('orders:checkout'))


class LoginFormView(RequestFormAttachMixin, FormView):
    form_class = LoginForm
    success_url = '/'
    template_name = 'accounts/auth.html'

    def form_valid(self, form):
        return super().form_valid(form)


class RegisterFormView(CreateView):
    template_name = 'accounts/registration.html'
    form_class = SignUpForm
    success_url = reverse_lazy('accounts:login')

    def form_valid(self, form):
        form.save()
        return super().form_valid(form)

    def form_invalid(self, form):
        return redirect(reverse('accounts:registration'))


def logout_view(request):
    logout(request)
    return redirect(reverse('accounts:login'))
    # Redirect to a success page.
