from django.urls import path, include
from rest_framework.urlpatterns import format_suffix_patterns


from . import views


urlpatterns = [

    path('analytics/sales/', views.SalesView.as_view(), name='sales'),

    path('analytics/sales/data/', views.SalesAjaxView.as_view(), name='sales-data'),

]

app_name = 'analytics'

urlpatterns = format_suffix_patterns(urlpatterns)