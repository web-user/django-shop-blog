from django.urls import path, include
from django.contrib.auth.views import login
from django.contrib.auth.views import logout
from django.contrib.auth.views import logout_then_login
from rest_framework.urlpatterns import format_suffix_patterns
from django.conf.urls import handler404, handler500

from landing import views


urlpatterns = [
    path('', views.HomeLandingView.as_view(), name='home_landing'),
    path('contact/', views.ContactPageView.as_view(), name='contact'),


]

app_name = 'landing'



urlpatterns = format_suffix_patterns(urlpatterns)