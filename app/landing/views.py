from django.views.generic import FormView, ListView, TemplateView
from django.urls import reverse_lazy, reverse
from django.http import JsonResponse, HttpResponse
from .forms import ContactPageForm
from products.models import Product, ProductCategory
from shops.models import Shop, LocationShop
from todo.models import Post

from django.core.mail import EmailMultiAlternatives



class HomeLandingView(TemplateView):

    template_name = 'landing/home_content.html'

    def get_context_data(self, **kwargs):
        kwargs['page_title'] = 'Page - Landing'
        kwargs['products'] = Product.objects.all()
        kwargs['products_category'] = ProductCategory.objects.filter(is_active=True)
        kwargs['posts'] = Post.objects.all()[:3]

        return super().get_context_data(**kwargs)


class ContactPageView(FormView):
    form_class = ContactPageForm

    template_name = 'landing/contact-page.html'
    success_url = reverse_lazy('landing:contact')

    def get_context_data(self, **kwargs):
        kwargs['shops'] = LocationShop.objects.filter(shop__is_active=True)

        return super().get_context_data(**kwargs)


    def form_valid(self, form):
        # We make sure to call the parent's form_valid() method because
        # it might do some processing (in the case of CreateView, it will
        # call form.save() for example).



        response = super().form_valid(form)
        if self.request.is_ajax():

            email = form.cleaned_data.get('email')
            fullname = form.cleaned_data.get('fullname')
            content = form.cleaned_data.get('content')

            subject, from_email, to = 'hello', email, 'pivnychenko.light.it@gmail.com'
            text_content = 'This breeee is an important message. '
            html_content = '<p>This is an <strong>important</strong> message. {} Name: {}</p>'.format(content, fullname)
            msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
            msg.attach_alternative(html_content, "text/html")
            msg.send()

            data = {
                'data': form.cleaned_data,
                'url': self.success_url,
                'message': "Thank you for your submission!"
            }
            return JsonResponse(data, status=200)
        else:
            return response

    def form_invalid(self, form):
        response = super().form_valid(form)
        errors = form.errors.as_json()
        if self.request.is_ajax():
            return HttpResponse(errors, status=400, content_type='application/json')
        else:
            return response
