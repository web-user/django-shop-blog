from django.urls import path, include
from rest_framework.urlpatterns import format_suffix_patterns
from django.conf.urls import handler404, handler500

from products import views


urlpatterns = [
    path('product/<slug:slug>/', views.ProductsDetailView.as_view(), name='product_detail'),
    path('products/API/', views.ProductRestApi.as_view(), name='product_api'),
    path('shop/', views.ShopProductListView.as_view(), name='shop'),

    path('product/<slug>/<pk>/', views.ProductDownloadView.as_view(), name='download'),

]

app_name = 'products'

urlpatterns = format_suffix_patterns(urlpatterns)