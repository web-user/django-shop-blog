from .models import Product, ProductCategory
from django.forms import ModelForm
from django import forms
import django_filters


class ProductApiFilter(django_filters.FilterSet):

    date = django_filters.DateFilter(name='updated', lookup_expr='gte')
    end_date = django_filters.DateFilter(name='updated', lookup_expr='lte')

    # total_price = django_filters.RangeFilter(name='price')


    price__gte = django_filters.NumberFilter(field_name='price', lookup_expr='gt', label='Minimum price')

    price__lte = django_filters.NumberFilter(field_name='price', lookup_expr='lte', label='Maximum price')

    category = django_filters.ModelMultipleChoiceFilter(
        queryset=ProductCategory.objects.all()
    )


    class Meta:
        model = Product
        fields = ('date', 'end_date', 'category', 'price__gte', 'price__lte' )


