import os
from zipfile import ZipFile
from django.forms import forms
from django.conf import settings
from django.core.exceptions import FieldError
from django.db import IntegrityError

from django.db import models
from django.utils.text import slugify
from autoslug import AutoSlugField
from django.urls import reverse
from django.db.models import Q
from tinymce.models import HTMLField
from django.db.models.signals import pre_save, post_save
from .extra import ContentTypeRestrictedFileField
from django.utils.translation import ugettext_lazy as _


from myproject.utils import unique_slug_generator, get_filename

# Create your models here.
class PublishedManager(models.Manager):

    def get_queryset(self):
        return super(PublishedManager, self).get_queryset()\
                .filter(status='published')


class ProductQuerySet(models.query.QuerySet):

    def active(self):
        return self.filter(is_active=True)

    def search(self, query):
        lookups = (Q(name__icontains=query) |
                  Q(description__icontains=query) |
                  Q(price__icontains=query)
                  )
        # tshirt, t-shirt, t shirt, red, green, blue,
        return self.filter(lookups).distinct()


class ProductManager(models.Manager):

    def get_queryset(self):
        return ProductQuerySet(self.model, using=self._db)

    def count_filter_all(self, number):
        return self.get_queryset().active()[:number]

    def count_filter(self, start, end):
        return self.get_queryset().active()[start:end]

    def all(self):
        return self.get_queryset().active()

    def get_by_id(self, id):
        qs = self.get_queryset().filter(id=id) # Product.objects == self.get_queryset()
        if qs.count() == 1:
            return qs.first()
        return None

    def search(self, query):
        return self.get_queryset().active().search(query)


class ProductCategory(models.Model):
    name            = models.CharField(max_length=64, blank=False, null=True, default=None)
    description     = models.TextField(blank=True, null=True, default=None)
    is_active       = models.BooleanField(default=True)
    created         = models.DateTimeField(auto_now_add=True, auto_now=False)
    updated         = models.DateTimeField(auto_now_add=False, auto_now=True)

    objects = models.Manager()

    def __str__(self):
        return "{}".format(self.name)


class Attributes(models.Model):
    name            = models.CharField(max_length=64, blank=False, null=True, default=None)
    description     = models.TextField(blank=True, null=True, default=None)
    is_active       = models.BooleanField(default=True)
    created         = models.DateTimeField(auto_now_add=True, auto_now=False)
    updated         = models.DateTimeField(auto_now_add=False, auto_now=True)

    objects = models.Manager()

    def __str__(self):
        return "{}".format(self.name)


class Product(models.Model):
    name            = models.CharField(max_length=164, blank=False, null=True, default=None)
    slug            = models.SlugField(blank=True, unique=True)
    category        = models.ManyToManyField(ProductCategory, related_name='categores', blank=True)
    price           = models.DecimalField(max_digits=10, decimal_places=2, default=0)
    discount        = models.IntegerField(default=0)
    is_active       = models.BooleanField(default=True)
    slider          = models.BooleanField(default=False)
    description     = HTMLField(default='', )
    created         = models.DateTimeField(auto_now_add=True, auto_now=False)
    updated         = models.DateTimeField(auto_now_add=False, auto_now=True)
    sku_product     = models.CharField(max_length=164, blank=True, null=True, default=None)

    objects = ProductManager()

    def get_absolute_url(self):
        #return "/products/{slug}/".format(slug=self.slug)
        return reverse("products:product_detail", kwargs={"slug": self.slug})

    class Meta:
        ordering = ('-updated',)

    def __str__(self):
        return "{}".format(self.name)

    def get_downloads(self):
        qs = self.productfile_set.all()
        return qs


def upload_product_file_loc(instance, filename):
    slug = instance.product.slug
    #id_ = 0
    id_ = instance.id

    if id_ is None:
        Klass = instance.__class__
        qs = Klass.objects.all().order_by('-pk')
        if qs.exists():
            id_ = qs.first().id + 1
        else:
            id_ = 0
    if not slug:
        slug = unique_slug_generator(instance.product)
    location = "products/product-file/"
    return location + filename #"path/to/filename.mp4"


class ProductFile(models.Model):
    product = models.ForeignKey(Product, on_delete=models.CASCADE, default=None)
    file    = models.FileField(upload_to=upload_product_file_loc, default='')

    objects = models.Manager()

    def __str__(self):
        return str(self.file.name)

    @property
    def display_name(self):
        og_name = get_filename(self.file.name)
        return og_name

    def get_default_url(self):
        return self.product.get_absolute_url()

    def generate_download_url(self):
        return self.file.url

    def get_download_url(self): # detail view
        return reverse("products:download", kwargs={"slug": self.product.slug, "pk": self.pk})


def product_pre_save_receiver(sender, instance, *args, **kwargs):

    if not instance.slug:
        instance.slug = unique_slug_generator(instance)

pre_save.connect(product_pre_save_receiver, sender=Product)


class ProductAttribut(models.Model):
    attribut        = models.ForeignKey(Attributes, on_delete=models.CASCADE, null=True, default=None)
    description     = models.CharField(max_length=164,  blank=True, null=True, default=None)
    product         = models.ForeignKey(Product, on_delete=models.CASCADE, related_name='attributes', blank=True, null=True, default=None)
    is_active       = models.BooleanField(default=True)
    created         = models.DateTimeField(auto_now_add=True, auto_now=False)
    updated         = models.DateTimeField(auto_now_add=False, auto_now=True)

    objects = models.Manager()

    def __str__(self):
        return "{}".format(self.attribut.name)


class ProductImage(models.Model):
    product         = models.ForeignKey(Product, on_delete=models.CASCADE, related_name='images', blank=True, null=True, default=None)
    image           = models.ImageField(upload_to='products/%Y/%m/', height_field=None, width_field=None, max_length=100)
    description     = models.CharField(max_length=164, blank=True, null=True, default=None)
    is_active       = models.BooleanField(default=True)
    is_main         = models.BooleanField(default=False)
    created         = models.DateTimeField(auto_now_add=True, auto_now=False)
    updated         = models.DateTimeField(auto_now_add=False, auto_now=True)

    objects = models.Manager()

    def __str__(self):
        return "{}".format(self.image.url)
