import requests
import time
import re
from django.views.generic import FormView, ListView, TemplateView, DetailView
from products.models import Product, Attributes, ProductCategory, ProductFile
from django.shortcuts import get_object_or_404
from django.contrib.auth.mixins import LoginRequiredMixin
from .forms import ProductApiFilter
from .serializers import ProductApiSerializer
import django_filters
from rest_framework import routers, serializers, viewsets, filters
from rest_framework import generics

from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from analytics.mixins import ObjectViewedMixin

from rest_framework import generics, mixins, permissions

from .api.pagination import StandardResultsPagination

from django.contrib import messages
from django.http import Http404, HttpResponse, HttpResponseRedirect
from django.views.generic import ListView, DetailView, View
from django.shortcuts import render, get_object_or_404, redirect
from django.conf import settings


class ProductRestApi(generics.ListAPIView, LoginRequiredMixin,  mixins.CreateModelMixin):
    authentication_classes = (SessionAuthentication, BasicAuthentication)
    pagination_class = StandardResultsPagination
    # permission_classes = (IsAuthenticated,)
    queryset = Product.objects.order_by('updated')
    serializer_class = ProductApiSerializer
    filter_class = ProductApiFilter
    search_fields = ('name',)
    ordering_fields = ('name', 'price', 'discount', 'category')
    filter_backends = (
        django_filters.rest_framework.DjangoFilterBackend,
        filters.SearchFilter,
        filters.OrderingFilter,
    )

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)

    # def perform_create(self, serializer):
    #     serializer.save(user=self.request.user)


class ProductsDetailView(ObjectViewedMixin, DetailView):
    model = Product
    template_name = 'products/content.html'

    def get_context_data(self, **kwargs):
        slug = self.kwargs.get('slug')
        my_object = get_object_or_404(Product, slug=slug)
        list_attr_item = Attributes.objects.filter(productattribut__product=my_object)

        kwargs['product'] = Product.objects.filter(slug=slug)
        kwargs['product_attr'] = list(set(list_attr_item))
        kwargs['page_title'] = re.sub(r'[/%-@*]', ' ', slug)

        # self.request.session.set_expiry(60)
        session_key = self.request.session.session_key
        if not session_key:
            self.request.session.cycle_key()

        return super().get_context_data(**kwargs)


class ShopProductListView(ListView):
    model = Product
    template_name = 'products/shop/content.html'
    context_object_name = 'products'  # Default: object_list
    paginate_by = 2
    queryset = Product.objects.all()  # Default: Model.objects.all()

    def get_context_data(self, **kwargs):
        kwargs['products_category'] = ProductCategory.objects.filter(is_active=True)
        return super().get_context_data(**kwargs)


class ProductDownloadView(View):
    def get(self, request, *args, **kwargs):
        slug = kwargs.get('slug')
        pk = kwargs.get('pk')
        downloads_qs = ProductFile.objects.filter(pk=pk, product__slug=slug)
        if downloads_qs.count() != 1:
            raise Http404("Download not found")
        download_obj = downloads_qs.first()
        # permission checks

        user_ready = True

        if not request.user.is_authenticated:
            user_ready = False

        if not user_ready:
            messages.error(request, "You do not have access to download this item")
            return redirect(download_obj.get_default_url())

        aws_filepath = download_obj.generate_download_url()
        print(aws_filepath)

        return HttpResponseRedirect(aws_filepath)

        # file_root = settings.PROTECTED_ROOT
        # filepath = download_obj.file.path # .url /media/
        # final_filepath = os.path.join(file_root, filepath) # where the file is stored
        # with open(final_filepath, 'rb') as f:
        #     wrapper = FileWrapper(f)
        #     mimetype = 'application/force-download'
        #     gussed_mimetype = guess_type(filepath)[0] # filename.mp4
        #     if gussed_mimetype:
        #         mimetype = gussed_mimetype
        #     response = HttpResponse(wrapper, content_type=mimetype)
        #     response['Content-Disposition'] = "attachment;filename=%s" %(download_obj.name)
        #     response["X-SendFile"] = str(download_obj.name)
        #     return response
        # return redirect(download_obj.get_default_url())
