from django.contrib import admin
from .models import *
from ckeditor_widget.admin import CKEditorAdminMixin
from django.contrib import messages
from django.db import IntegrityError


from ckeditor_widget.widgets import CKEditorWidget

class CKEditorAdminMixin:
    formfield_overrides = {
        models.TextField: {'widget': CKEditorWidget}
    }

# Register your models here.

class ProductImageInline(admin.TabularInline):
    model = ProductImage
    extra = 0


class ProductFileInline(admin.TabularInline):
    model = ProductFile
    extra = 0

class ProductCategoryInline(admin.TabularInline):
    model = ProductCategory
    extra = 0


class ProductAttributInline(admin.TabularInline):
    model = ProductAttribut
    extra = 0


class AttributesInline(admin.TabularInline):
    model = Attributes
    extra = 0


class ProductAdmin(admin.ModelAdmin):

    list_display = ('name', 'slug', 'price', 'is_active', 'slider', 'updated', 'created')
    # readonly_fields = ('slug',)

    inlines = [ProductImageInline, ProductAttributInline, ProductFileInline]
    search_fields = ('name', 'description')


    # def save_model(self, request, obj, form, change):

    #     if 'Watches' in obj:
    #         messages.add_message(request, messages.INFO, 'Car has been sold')
    #     super().save_model(request, obj, form, change)

    class Meta:
        model = Product

admin.site.register(Product, ProductAdmin)


class ProductImageAdmin (admin.ModelAdmin):
    list_display = [field.name for field in ProductImage._meta.fields]

    class Meta:
        model = ProductImage

admin.site.register(ProductImage, ProductImageAdmin)


class ProductFileAdmin(admin.ModelAdmin):
    list_display = [field.name for field in ProductFile._meta.fields]

    class Meta:
        model = ProductFile

admin.site.register(ProductFile, ProductFileAdmin)


class ProductCategoryAdmin (admin.ModelAdmin):
    list_display = [field.name for field in ProductCategory._meta.fields]

    class Meta:
        model = ProductCategory

admin.site.register(ProductCategory, ProductCategoryAdmin)


class ProductAttributAdmin (admin.ModelAdmin):
    list_display = [field.name for field in ProductAttribut._meta.fields]

    class Meta:
        model = ProductAttribut

admin.site.register(ProductAttribut, ProductAttributAdmin)


class AttributesAdmin (admin.ModelAdmin):
    list_display = [field.name for field in  Attributes._meta.fields]

    class Meta:
        model = Attributes

admin.site.register(Attributes, AttributesAdmin)