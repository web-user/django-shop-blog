from .models import Product
from rest_framework import routers, serializers, viewsets

class ProductApiSerializer(serializers.ModelSerializer):

    class Meta:
        model = Product
        fields = ('id', 'name', 'slug', 'category', 'price',
                  'description', 'updated')