from django.conf import settings
from django.views.generic import UpdateView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.shortcuts import render

from .forms import MarketingPreferenceForm
from .models import MarketingPreference

MAILCHIMP_EMAIL_LIST_ID = getattr(settings, "MAILCHIMP_EMAIL_LIST_ID", None)


class MarketingPreferenceUpdateView(LoginRequiredMixin, SuccessMessageMixin, UpdateView):
    form_class = MarketingPreferenceForm
    template_name = 'marketing/forms.html'
    success_url = '/settings/email/'
    success_message = "Your email preference have been update."

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        return context

    def get_object(self):
        user = self.request.user
        obj, created = MarketingPreference.objects.get_or_create(user=user)
        return obj



""" 
data[merges][ADDRESS]:

data[merges][EMAIL]: pivnychenko.light.it@gmail.com

data[ip_opt]: 91.214.31.37

data[id]: 560762dd57

fired_at: 2018-09-05 08:46:59

data[email]: pivnychenko.light.it@gmail.com

data[merges][FNAME]:

data[merges][LNAME]:

data[web_id]: 238377933

data[list_id]: c04c7c2a2b

data[email_type]: html

type: subscribe

data[merges][PHONE]:
"""

def mailchimp_webhook_view(request):
    data = request.POST
    list_id = data.get('data[list_id]')
    email = data.get('data[email]')
    hook_type = data.get('type')
    return ''