from django.urls import path, include
from rest_framework.urlpatterns import format_suffix_patterns


from . import views

urlpatterns = [
    path('settings/email/', views.MarketingPreferenceUpdateView.as_view(), name='marketing_pref'),

]

app_name = 'marketing'



urlpatterns = format_suffix_patterns(urlpatterns)